# Why Java is bad?

## Missing things from framework
- There is no `Integer.tryParseInt` or similar API (because the language does not support out/ref parameters like C#).

## Language defects
- ["how to instanceof List\<MyType\>?"](https://stackoverflow.com/questions/10108122/how-to-instanceof-listmytype) -> _"That is not possible because the datatype erasure at compile time of generics"_

## Inconvenience
- stream() API is a lot more inconvenient than LINQ
