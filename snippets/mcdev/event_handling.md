# Event handling

- Handler class must implement `Listener` (`org.bukkit.event.Listener`, marker interface)

**Specify and EventHandler:**
```java
@EventHandler
public void whatever(AsyncPlayerChatEvent event) {
    // Do something
}
```
The method's name does not matter.

## Priority
To explicitly specify a priority:
```java
@EventHandler(priority = EventPriority.LOWEST)
```
**Default is `NORMAL`**

Handlers are called in the following order:
```
1. LOWEST
2. LOW
3. NORMAL
4. HIGH
5. HIGHEST
6. MONITOR
```
So for early cancellation, `LOWEST` is recommended.

## Cancellation
To cancel an event, simply:
```java
event.setCancelled(true);
```

If you want to get your event handler executed even if it has been canceled previously:
```java
@EventHandler(ignoreCancelled = true)
```
Of course, default is `false`.

# Registration
Use the `PluginManager` to register events. In your Plugin class:
```java
// Register
YourListenerImplementation listener = new new YourListenerImplementation();
getServer().getPluginManager().registerEvents(listener, this);

// Unregister
HandlerList.unregisterAll(listener); // (org.bukkit.event.HandlerList)
```