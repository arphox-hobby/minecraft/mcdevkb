# PacketPlayOutCommands / "Declare commands" with ProtocolLib

`PacketPlayOutCommands` is the name of packet spigot (1.13+) sends when a player joins the server to let the player know which commands are available for the player.  
This list can be manipulated by the [`PlayerCommandSendEvent`](articles/events/PlayerCommandSendEvent.md).

[**Declare_Commands**](https://wiki.vg/Protocol#Declare_Commands)

![](https://i.imgur.com/l4aXwzS.png)

### Prerequisites
```xml
<repositories>
    <repository>
        <id>dmulloy2-repo</id>
        <url>https://repo.dmulloy2.net/nexus/repository/public/</url>
    </repository>
    <repository>
        <id>minecraft-libraries</id>
        <name>Minecraft Libraries</name>
        <url>https://libraries.minecraft.net</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>com.comphenix.protocol</groupId>
        <artifactId>ProtocolLib</artifactId>
        <version>4.5.0</version>
    </dependency>
    <dependency>
        <groupId>com.mojang</groupId>
        <artifactId>brigadier</artifactId>
        <version>1.0.500</version>
    </dependency>
</dependencies>
```

# Packet listener example

```java
ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();

PacketAdapter packetListener = new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Server.COMMANDS) {
    @Override
    public void onPacketSending(PacketEvent event) {
        RootCommandNode rootCommandNode = (RootCommandNode) event.getPacket().getModifier().read(0);
        Collection<LiteralCommandNode> children = rootCommandNode.getChildren();
        Collection<String> commands = children.stream().map(node -> node.getLiteral()).collect(Collectors.toCollection(ArrayList::new));
    }
};

protocolManager.addPacketListener(packetListener);
```

# Packet sending example
```java
private void test(Player player) {
    ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();

    RootCommandNode rootNode = new RootCommandNode();
    rootNode.addChild(createLiteralCommandNode("iam"));
    rootNode.addChild(createLiteralCommandNode("so"));
    rootNode.addChild(createLiteralCommandNode("happy"));

    PacketContainer packetContainer = protocolManager.createPacket(PacketType.Play.Server.COMMANDS, true);
    packetContainer.getModifier().write(0, rootNode);

    try {
        protocolManager.sendServerPacket(player, packetContainer);
    } catch (InvocationTargetException e) {
        throw new RuntimeException("Cannot send packet " + packetContainer, e);
    }
}

private LiteralCommandNode createLiteralCommandNode(String value) {
    return new LiteralCommandNode(value, null, null, null, null, false);
}
```
Result: ![](https://i.imgur.com/76qDjpt.png)