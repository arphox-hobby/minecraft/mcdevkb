**Documentation**
- https://docs.adventure.kyori.net/minimessage/index.html

**Format example**
- `<red>Hopper</red> <blue><u>1</u></blue>`

**Parsing in code**
```java
Component displayName = MiniMessage.miniMessage().deserialize("...");
```
