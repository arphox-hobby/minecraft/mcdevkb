# Useful
- [HelpChat](https://helpch.at/)
- [server.properties](https://minecraft.gamepedia.com/Server.properties)

# Tutorials
## Bukkit
[Plugin Tutorial](https://bukkit.gamepedia.com/Plugin_Tutorial)
## Spigot
### Wiki
[**Plugin development**](https://www.spigotmc.org/wiki/spigot-plugin-development/)  
[**Plugin Snippets**](https://www.spigotmc.org/wiki/plugin-snippets/)  
[**BungeeCord Plugin Development**](https://www.spigotmc.org/wiki/bungeecord-plugin-development/)  
[FAQ](https://www.spigotmc.org/wiki/faq/)  
[Spigot Plugin Development subpage](https://www.spigotmc.org/wiki/spigot-plugin-development/)  

# Setup
[IntelliJ IDEA](https://www.jetbrains.com/idea/download/)  
[Java SE JDK 8u51](https://download.oracle.com/otn/java/jdk/8u51-b16/jdk-8u51-windows-x64.exe) [link2](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)  
[Spigot API javadoc](https://hub.spigotmc.org/nexus/content/repositories/snapshots/org/spigotmc/spigot-api/)  

# Java
[**Code Conventions for the Java Programming Language: 9. Naming Conventions**](https://www.oracle.com/technetwork/java/codeconventions-135099.html)
## Basics
[Trail: Collections](https://docs.oracle.com/javase/tutorial/collections/index.html)

# Useful resources
**Servers**:
- [Spigot](https://getbukkit.org/download/spigot)
- [Paper](https://papermc.io/): [⬇](https://papermc.io/downloads) [GitHub](https://github.com/PaperMC/Paper) [Forum](https://papermc.io/forums/) [Documentation](https://paper.readthedocs.io/en/latest/)

plugin.yml: [Bukkit](https://bukkit.gamepedia.com/Plugin_YAML) [Spigot](https://www.spigotmc.org/wiki/plugin-yml/) [Annotations](https://www.spigotmc.org/wiki/spigot-plugin-yml-annotations/)

# Plugins
[Plugman](https://dev.bukkit.org/projects/plugman)