# `spigot.yml`
## Links
- [Spigot Configuration (spigot.yml)](https://www.spigotmc.org/wiki/spigot-configuration/)
- [Optimizing spigot.yml to Reduce Lag - Shockbyte](https://shockbyte.com/billing/knowledgebase/153/Optimizing-spigotyml-to-Reduce-Lag.html)
- [[GUIDE] Server Optimization](https://www.spigotmc.org/threads/guide-server-optimization%E2%9A%A1.283181/)

## My recommendations
**If you can't find an option in this list that is because I recommend using the default value.**
- `settings:`
  - `sample-count`: I recommend setting this to `0`, but if you find it useful, leave it as it is.
  - `save-user-cache-on-stop-only`: Set this to `true` (unless your server crashes often). This reduces disk I/O operations.
  - `netty-threads`: Increase it to 6 if your CPU has at least 8 logical cores.
Increase it to 8 if you usually have a **lot** of players (50+) **and** your CPU has at least 16 logical cores.
  - `user-cache-size`: It is nowhere mentioned but the number here means the number of players.
  - `bungeecord`: If you use BungeeCord/WaterFall, set to `true`, otherwise `false`.
[Read more here](https://www.spigotmc.org/threads/spigot-yml-bungeecord.10637/#post-103664).
  - `player-shuffle`: Keep it on 0. (it means turn this feature off).
(Otherwise, the lower the value, the bigger the performance cost is. If you have a "hardcore PVP" server orientated around fighting constantly, you may use this (e.g. set to 1200).)
  - `moved-wrongly-threshold`: If you experience a lot of your players making the server log "XYZ moved wrongly!" (it means they have latency problems),
consider increasing this value.
  - `moved-too-quickly-multiplier`: If you experience a lot of your players making the server log "XYZ moved too quickly!", consider increasing this value.
  - `log-villager-deaths`: `false`. It has quite little performance impact, though. If you are really curious why villagers die on your server, leave it on true.
- `world-settings`:
  - `item-despawn-rate`: Consider reducing it from the default 5 minutes (6000 ticks) if you want fewer entities, but 5 minutes sounds fine.
  - `arrow-despawn-rate`: Consider reducing it from the default 60 seconds (1200 ticks)
  - `hanging-tick-frequency`: Sets the hanging entities' (`ItemFrame`, `LeashHitch`, `Painting`) tick frequency.
Tick logic of these entities contain e.g. physics check (if you break the block behind them they should "break" too).
Increase this value if in your timings report, `Minecraft::tickEntity - nms.EntityItemFrame` part takes any significant amount of time. 
You can even reduce this number if you want better experience for your players.
  - `nerf-spawner-mobs`: If it is okay for you that mobs from mob spawners do nothing (no attack&move), 
     set it to `true` for a nice performance boost if there are a lot of spawners on your server.
  - `zombie-aggressive-towards-villager`: If it is okay for you that zombies will not attack villagers, set it to `false` for a (probably) minor performance boost.
  - `mob-spawn-range`: Set to `6`.
  - `max-tnt-per-tick`: The default `100` seems a little too much for me _per tick_,
feel free to reduce it if you want (the default allows exploding up to 2000 TNTs per second which is quite lot).
  - `merge-radius`:
    - `item`: 4
    - `exp`: 6
  - `max-tick-time`: **[learn why](https://aikar.co/2015/10/08/spigot-tick-limiter-dont-use-max-tick-time/)**
    - `tile`: 1000
    - `entity`: 1000
  - `entity-activation-range`: lower = better performance but worse gameplay experience. Before changing it, make sure to **understand** what is it!
    - `animals`: 16-32
    - `monsters`: 24-32
    - `raiders`: 48
    - `misc`: 8-16
