# Plugin object life-cycle

As I experienced:

## Plugin loading on server load
1. Loading starts, reads config, etc.
2. **LOADs plugins**
3. Loads worlds
4. Prepares spawn areas
5. **ENABLEs plugins**

But if you set `load: STARTUP` in `plugin.yml`, the server enables your plugin right after _loading_ it.

## Manual plugin loading [PlugMan]

**Important**:
- on `unload`, static (and so instance) context is lost
- on `disable`, instance context is **NOT** lost

### Load
1. `>plugman load YourPlugin`
2. YourPlugin `static constructor` is called
3. YourPlugin `instance constructor` is called
4. `onLoad()`
5. `onEnable()`

### Unload
1. `>plugman unload YourPlugin`
2. `onDisable()`

(There is no such thing as "onUnLoad")

### Enable
1. `>plugman enable YourPlugin`
2. `onEnable()`

### Disable
1. `>plugman disable YourPlugin`
2. `onDisable()`

## Events / commands / etc
- when your plugin is disabled, its
  - events are unregistered (this is why you register events at onEnable and _not_ onLoad)
  - command executors will not fire
    - when a command is executed while its handler plugin is disabled, an error occurs:  
    `org.bukkit.command.CommandException: Cannot execute command 'hello' in plugin HelloWorld2020 v1.0 - plugin is disabled.`  
    and the command sender (e.g. player) sees a message in red _"An internal error occurred while attempting to perform this command"_
  - scheduled tasks are canceled (e.g. if you use `Bukkit.getScheduler().runTaskTimerAsynchronously` since you pass in the plugin instance and the server can identify it)