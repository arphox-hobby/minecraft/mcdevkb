As I experienced, usually the item from the broken block is the last in the items list.
But be careful, because the block break may not yield the original block (if I break a hopper with bare hands, the hopper will be destroyed but its content will be dropped, and its content's last item can be a hopper too).
