# Limiting the number of hoppers in a world

With high performance, of course.

## Business requirements
As a server administrator, I would like to limit the number of existing hoppers in a world because a large number of hoppers affect server performance.  
In the end, I want every chunk to contain at most N (configurable) hoppers; but it does not have to be enforced every seconds or clicks.  
The solution has to be very efficient:
- effect on main thread should be minimized
- most of work should be done on a different thread (async)
- should be low on memory allocations and give as little work for GC as possible
  - but it is acceptable to initially allocate medium to large amount memory for long time (as the server has a lot of memory)

The performance priority is the following:
- CPU time on main thread
- CPU time on other threads
- memory allocations & work for GC
- long term memory allocations those are not cleaned up often

## Ideas / TODO
- per world operation?

# Technical research
Let us see our opportunities:

### Events
- **`ChunkLoadEvent`**: `getChunk()`, `getWorld()`
- **`ChunkUnloadEvent`**: `getChunk()`, `getWorld()`
- **`BlockPlaceEvent`**: `getPlayer()`, `getBlockPlaced()`, `getBlockAgainst()`
- **`BlockBreakEvent`**: `getPlayer()`, `getBlock()`

### Direct operations
- `Server.`
  - `getWorld(String name)`
  - `getWorld(UUID uuid)`
  - `getWorlds()`
- `World.`
  - `getLoadedChunks()`: returns `Chunk[]`. Implementation of this method is non-trivial (stream, filter, map, toArray).
- `Chunk.`
  - `Block getBlock(int x, int y, int z);`
  - `ChunkSnapshot getChunkSnapshot(boolean includeMaxBlockY, boolean includeBiome, boolean includeBiomeTempRain)`
  - `ChunkSnapshot getChunkSnapshot();`: Capture thread-safe read-only snapshot of chunk data. Calls `getChunkSnapshot(true, false, false)`.
    - zzzCat said that it is for _"doing expensive stuff off the main thread with the chunks data"_
  - `BlockState[] getTileEntities(boolean useSnapshot);`
  - `BlockState[] getTileEntities()`: this calls `getTileEntities(true)`
  - `boolean contains(@NotNull BlockData block);`: Implementation looks fast and linear

By contacting Aikar, the best solution is to do the followings in sync:
1. if chunk.contains(BlockData for Hopper), call getTileEntities(false) and count hoppers there.